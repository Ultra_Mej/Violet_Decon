# VIOLET DECON
Using light at 200-300nm (UV-C) for viral decontamination.  
This project is completely Open-Source. Go tell your friends!


## References & Related projects
--------------------------------------------------------------------------------

### 1/ https://www.n95decon.org/
A scientific consortium for data-driven study of N95 filtering facepiece respirator decontamination,
including [UVC irradiation](https://www.n95decon.org/s/200401_N95DECON_UV_factsheet_v12_final.pdf)

### 2/ https://www.frolicstudio.com/covid-19-decontamination-toolkit
Open-source DIY COVID-19 decontamination box using UVC

### 3/ https://www.nebraskamed.com/sites/default/files/documents/covid-19/n-95-decon-process.pdf
COVID-19 decontamination room using UVC

### 4/ http://www.needlab.org/face-masks-disinfection-device
Open-source Face-Mask Disinfection Device Using UV-C and Dry Heat

### 5/ http://www.iuva.org/COVID-19
Fact Sheet on UV Disinfection for COVID-19 by the IUVA "International" Ultraviolet Association

### 6/ https://www.iuvanews.com/stories/pdf/archives/180301_UVSensitivityReview_full.pdf
Review of the radian exposure needed for *Incremental Log Inactivation of Bacteria, Protozoa, Viruses and Algae needed IUVA*

### 7/ https://www.nature.com/articles/s41598-018-21058-w.pdf
Far-UVC light: A new tool to control the spread of airborne-mediated microbial diseases


## UltraViolet Germicidal Irradiation
--------------------------------------------------------------------------------
[UVGI](https://en.wikipedia.org/wiki/Ultraviolet_germicidal_irradiation) is an **old, field proven, FDA approved** technique that uses UV-C light (200-280nm) to inactivate a wide range of microorganisms.  
UV-C light kills virus by breaking (at the molecular level) their proteins and RNA; a phenomenon also known as **photo-degradation**.

Their are two kinds of UV-C lights sources used today in UVGI:
* Gas discharge light bulb
* UVC LED (Light Emitting Diode)



## Radiant exposure aka "UV dose"
--------------------------------------------------------------------------------

**NB: 1 J/cm² <=> 1W/cm² for 1s <=> 1mW/cm² for 20 minutes <=> 35µW/cm² for 8 hours**

In ref 1/ it is reported that radiant exposure **≥1 J/cm² of UV-C inactivates viruses similar to SARS-CoV-2 on a N95 mask**. I couldn't find the paper quoted "Mills et al., 2018; [2] Heimbuch & Harnish, 2019; [3] Lore et al., 2012; [4] Lin et al., 2018; [5] Fisher and Shaffer, 2010" to investigate (wavelength, level of inactivation, protocol...).

In ref 6/ the radiant exposure reported to reduce virus in cell is:
  * @ LED 285nm (p20/41): 3mJ/cm² for 1log reduction, 130mJ/cm² for 3log. Oguma et al. 2015
  * @ LED 255nm (p22/41): 14mJ/cm² for 1log, 38mJ/cm² for 3log. Aoyagi et al. 2011
  * @ LED 260nm (p23/41): 13mJ/cm² for 1log, 40mJ/cm² for 3log, 53mJ/cm² for 4log . Sholtes et al. 2016  
NB: Doesn't account for the penetration depth of light in material.  

In ref 7/ it is reported that **2mJ/cm² of 222nm light inactivating >95% of aerosolized H1N1 influenza virus**  



## Gas discharge light bulbs
--------------------------------------------------------------------------------
19th century technology. A gas (mercury for 250nm, excimers for more exotic/expensive choices...) trapped in a quartz bulb (glass absorbs UV light) is exited by an electric arc and emits UV light.
Examples:

[Light bulb](https://www.lighting.philips.com/main/prof/conventional-lamps-and-tubes/special-lamps/purificationwater-and-air/commercial-and-professional-air/tuv-pl-l/927908704007_EU/product) used in ref 2/:
  - 4 x 53 x 2cm, 130g
  - Main peak at 250nm
  - 17W radiant power => ~ 2mW/cm² at 20cm @ 55W power supply (100V, 500mA)
  - [30€/unit](https://www.lamps-on-line.com/tuv-pl-l-55w-4p-germicidal-pll-55w-2g11-uvc.html)
  - volume prices? 10€/unit? **!TBD!**

[Light bulb](https://www.amazon.fr/gp/product/B00XY9IUFG/) used in ref 4/:
  -  11 W lamp bulb from a “Sterilizer for Aquarium” kit
  -  similar specs as above. Optical power divided by 3?
  - [50€/unit](https://www.amazon.fr/gp/product/B00XY9IUFG/)

[Light bulb system](https://www.clordisys.com/torch.php) used in ref 3/:
  - 60 x 170 x 60cm, 30kg
  - 200µW/cm² at 3m @ 1kW power supply (220V/50Hz, 6A)
  - 25k€/unit



## UVC LED (Light Emitting Diode)
--------------------------------------------------------------------------------
LED: 20th century technology. Electrons jump a bandgap and emit photons. The bandgap (the wavelength of the photon) is engineered by carefully doping semiconductors.  
UVC LED: 21st century technology. Aka "deep UV" LED. [AlGaN](https://en.wikipedia.org/wiki/Aluminium_gallium_nitride). Mostly used in the industry for UV curing.

### 280nm LED
The cheapest and the 'easiest' to buy

[RVXR-280](https://www.y-ic.com/datasheet/bc/RVXR-280-SB-073105.pdf)
  - 3 x 3 x 2.5mm, SMD package, integrated dome lens
  - Peak wavelength 280nm, FWHM 12nm, 80% power in +/- 60°
  - Output Radiant Power 8mW => ~100µW/cm² at 30mm (over 100cm²) @ 1W power supply (100mA, 7V forward voltage)
  - [6€/unit](https://www.digikey.fr/product-detail/en/rayvio-corporation/RVXR-280-SM-073105/1807-1023-1-ND/8635409?cur=EUR&lang=en)
  - volume prices?  **!TBD!**

[CUD7GF1A](https://www.neumueller.com/en/artikel/cud7gf1a)
  - 4 x 4 x 1mm, SMD package
  - Peak wavelength 280nm, FWHM 11nm, , 80% power in +/- 60°
  - Optical output power 2mW => ~20µW/cm² at 30mm (over 100cm²) @ 0.1W power supply (20mA, 6V forward voltage)
  - [9€/unit](https://store.nacsemi.com/Products/Detail?auth=n&utm_campaign=listing&part=CUD7GF1A&utm_medium=aggregator&Mfr=SEOUL%20VIOSYS%20CO.%20LTD&stock=XSFP00000084080&utm_source=findchips&utm_content=textlink)
  - volume prices? [5€/unit](https://www.aliexpress.com/i/33020883837.html?spm=2114.12057483.0.0.5bfc70a4YDNhaZ) **!TBC!**

[XST-3535-UV](https://www.luminus.com/products/uv)
    - 4 x 4 x 3mm, SMD package, integrated dome lens
    - Peak wavelength 280nm, FWHM 10nm, 80% power in +/- 30°
    - Output Radiant Power 45mW => ~3mW/cm² at 30mm (over 12cm²) @ 2W power supply (350mA, 5V to 7V forward voltage)
    - [24€/unit for 100 units](https://www.mouser.fr/ProductDetail/Luminus-Devices/XST-3535-UV-A60-CD275-00?qs=sGAEpiMZZMusoohG2hS%252B13XB79dZiCCbkzvwIpDy1yrlfg%2F1mtgJiw%3D%3D)

[NCSU334A](https://www.led-professional.com/products/uv-ir-components/nichia-launches-a-280nm-deep-uv-led)
  - 7 x 7 x 2mm, 0.3g, SMD package
  - Peak wavelength 280nm, FWHM 10nm, 80% power in +/- 60°
  - "Radiant flux" 50mW  => ~0.4mW/cm² at 30mm (over 100cm²) @ 2W power supply (350mA, 5V forward voltage)
  - [160€/unit](https://www.aliexpress.com/i/33021503870.html) **what??**
  - volume prices?  **!TBD!**

### 260nm LED
More expensive and harder to get. **Under construction / Work needed**  

### 240nm LED
More expensive and harder to get. **Under construction / Work needed**  

### 220nm LED
More expensive and harder to get. **Under construction / Work needed**  



## UVC LASER
--------------------------------------------------------------------------------

[Oxxius Deep UV](https://www.photonicsolutions.co.uk/product-detail.php?prod=6414)
  - 220 x 161 x 110 mm, laser head with heatsink
  - 266nm / 280nm
  - 10mW DC
  - 10k€+ **!TBC!**



## End Game
--------------------------------------------------------------------------------
A little irradiation box with **BOM < 100€** and **low electric consumption** to decontaminate small and flat objects that would be damaged by soap & water, such as:
  * Disposable masks
  * filters of reusable masks
  * Money bills
  * ...  


## Experimental investigations
--------------------------------------------------------------------------------
**Under construction / Work needed**   

### What is the **real** minimal quantity of UVC photons needed to inactivate a virus? to inactivate sars-cov-2?

#### How does it translate in terms of irradiation-time and BOM-cost for different optical parameters?
Optical parameters to be studied:
* wavelength in the order of their cost and availability:
  - 405nm LED and Laser diode
  - 280nm LED,
  - 252nm Mercury bulb
  - other exotic LEDs and LASERs: 260nm, 220nm
* Continuous power: @ max DC supply and @ max/10
* Modulated power: 10% duty cycle @ max DC supply (ie peak irradiance 10x higher, but during a 10th of the time)

#### Can we measure sars-cov-2 virulence?
As opposed as only measuring
* sars-cov-2 RNA concentration
* sars-cov-2 spike proteins concentration
* ...

Different wavelengths will breakdown different molecular bounds of the virus with different efficiency.
<img src="/pictures/sars-cov-2_structure.jpg" width="500">  

We don't need to **kill** but only to **inactivate**. Overspec would mean overprice and/or over-complicated.  
Exemple: the soap innactivates the virus by breakingdown its membrane. Not i

The **sars cov protocol virulence protocol** is yet TBD but should involve:



NB: such a **propagative** work would require a **BSL 3** laboratory according to the [WHO guidelines](https://apps.who.int/iris/bitstream/handle/10665/331138/WHO-WPE-GIH-2020.1-eng.pdf?sequence=1&isAllowed=y) and [CDC guidelines](https://www.cdc.gov/coronavirus/2019-ncov/lab/biosafety-faqs.html).



## Theoretical investigations
--------------------------------------------------------------------------------
**Under construction / Work needed**  

### What is the absorption spectra of the SARS-CoV-2??
It will absorb a lot bellow 300nm, ok. We now.  
But what about the NUV-VIS absorption spectra? Saturating a peak of absorption could initiate breakdown...

From this [talk on "UVGI Fundamentals and Indoor Environment Applications"](https://www.youtube.com/watch?v=uqSHPKicIbs) we can get this "spectra":  
<img src="/pictures/UVGI_spectral_efficiency_Tallk_byWilliam-Bahnfleth_DTUdk.jpg" width="500">  
Which may be the RNA absorption spectra.
Is it inconsistent with the reported efficiency of 220nm (few mJ when 260nm requires from 100 to 1000mJ)?
What about the proteins (spike, nucleocapsid, ribosome-hacking...) absorption spectra?
405nm @~3kJ innactivates Feline CaliciVirus

### Is it only the J/cm² that maters or the pulsed light is better that DC light at equivalent power? (non linear DNA/RNA/Protein breakdown with light intensity)
[Efficacy of Pulsed 405-nm Light-Emitting Diodes for Antimicrobial Photodynamic Inactivation: Effects of Intensity, Frequency, and Duty Cycle](https://www.researchgate.net/publication/309301352_Efficacy_of_Pulsed_405-nm_Light-Emitting_Diodes_for_Antimicrobial_Photodynamic_Inactivation_Effects_of_Intensity_Frequency_and_Duty_Cycle), 2016

### Could Multiwavelength excitations reduce the UV dose neeeded for viral inactivation?
by multi photon absorption (exited states absorption spectra of fluroescent Amino Acid to be investigated 1st!!)
